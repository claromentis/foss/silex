<?php

/*
 * This file is part of the Silex framework.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Silex\Provider\Routing;

use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\RequestContext;

/**
 * Implements a lazy UrlMatcher.
 *
 * @author Igor Wiedler <igor@wiedler.ch>
 * @author Jérôme Tamarelle <jerome@tamarelle.net>
 */
class LazyUrlMatcher implements UrlMatcherInterface
{
    private $factory;

    public function __construct(\Closure $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Returns the corresponding UrlMatcherInterface instance.
     *
     * @return UrlMatcherInterface
     */
    public function getUrlMatcher()
    {
        $urlMatcher = call_user_func($this->factory);

        if (!$urlMatcher instanceof UrlMatcherInterface) {
            throw new \LogicException("Factory supplied to LazyUrlMatcher must return implementation of Symfony\Component\Routing\Matcher\UrlMatcherInterface.");
        }

        return $urlMatcher;
    }

    public function match(string $pathinfo)
    {
        return $this->getUrlMatcher()->match($pathinfo);
    }

    public function setContext(RequestContext $context)
    {
        $this->getUrlMatcher()->setContext($context);
    }

    public function getContext()
    {
        return $this->getUrlMatcher()->getContext();
    }
}
